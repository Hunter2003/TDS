// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSHealthComponent.h"
#include "TDSCharacterHealthComponent.generated.h"

/**
 *
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldBroken);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldRepared);

UCLASS()
class TDS_API UTDSCharacterHealthComponent : public UTDSHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnShieldChange OnShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnShieldBroken OnShieldBroken;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnShieldRepared OnShieldRepared;


	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;
protected:

	

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float Shield = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.01f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		bool IsShield = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		bool IsRecoveringShield = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		bool IsRecoveringShieldEnable = true;

	//Spawn new shiel and destroy old

	UFUNCTION(BlueprintCallable)
		void ChangeCharacterHealthValue(float ChangeValue);

	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();

	void RecoveryShield();
};

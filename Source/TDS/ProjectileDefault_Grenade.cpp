// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"


void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

	
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);

	DrawDebugSphere
	(

		NULL,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		12,
		FColor::Red,
		false,
		1.f,
		100,
		10.f

	);

	DrawDebugSphere
	(

		NULL,
		GetActorLocation(),
		ProjectileSetting.ProjectileMaxRadiusDamage,
		12,
		FColor::Blue,
		false,
		1.f,
		100,
		10.f

	);

}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::InitProjectile_Gren(FProjectileInfo InitParam)
{
	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileInitSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	ProjectileSetting = InitParam;

	BulletMesh->SetStaticMesh(InitParam.BulletMeshT);
	BulletFX->SetTemplate(InitParam.BulletFXT);
}

void AProjectileDefault_Grenade::Explose()
{
	TimerEnabled = false;

	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;

// 	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
// 		ProjectileSetting.ExploseMaxDamage,
// 		ProjectileSetting.ExploseMaxDamage * 0.2f,
// 		GetActorLocation(),
// 		1000.0f,
// 		2000.0f,
// 		5,
// 		NULL, IgnoredActor, nullptr, nullptr);

	ECollisionChannel n = ECC_GameTraceChannel2;			//2, 

 	bool a = UGameplayStatics::ApplyRadialDamageWithFalloff
	(
		
		GetWorld(),
		ProjectileSetting.ExploseMaxDamage,					//Base damage
		0,													//Minimum Damage
		GetActorLocation(),									//Origin
		ProjectileSetting.ProjectileMinRadiusDamage,		//Inner radius
		ProjectileSetting.ProjectileMaxRadiusDamage,		//Outer radius
		ProjectileSetting.ExplodeFalloffCoef,				//Damage Falloff
		NULL,												//Damage type
		IgnoredActor,										//Ignored actor
		this,		//Damage causer
		nullptr,
		n
	);

	this->Destroy();
}
